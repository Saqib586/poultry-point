﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public partial class Add_Category : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCatRptr();
        }
    }
    private void BindCatRptr()
    {
        try
        {
            String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                using (SqlCommand cmd = new SqlCommand("select * from tblCategories", con))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        DataTable dtCat = new DataTable();
                        sda.Fill(dtCat);
                        rptrCat.DataSource = dtCat;
                        rptrCat.DataBind();


                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblerror.Text = (ex.Message);
            lblerror.ForeColor = System.Drawing.Color.Red;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("insert into tblCategories values('" + txtCatName.Text + "')", con);
                if (txtCatName.Text == "")
                {
                    lblerror.Text = " All Fields Required";
                    lblerror.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    //   txtCatName.Text = string.Empty;
                }
            }
            BindCatRptr();
        }
        catch(Exception ex)
        {
            lblerror.Text = (ex.Message);
            lblerror.ForeColor = System.Drawing.Color.Red;
        }
    }
}