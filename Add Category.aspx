﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="Add Category.aspx.cs" Inherits="Add_Category" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="form-horizontal">
            <h2>Add Category</h2>
            <hr />
            <div class="form-group">
                <asp:Label ID="Label1" CssClass="col-md-2 control-label " runat="server" Text="Category Name"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox ID="txtCatName" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorProName" ValidationGroup="error" CssClass="text-danger" ControlToValidate="txtCatName" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
            </div>
             <div class="form-group">
                    <asp:Label ID="lblimg" CssClass="col-md-2 control-label " runat="server" Text="Upload Image"></asp:Label>
                <div class="col-md-3">
                    <asp:FileUpload ID="fuImg" runat="server" CssClass="form-control" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="error" CssClass="text-danger" ControlToValidate="fuImg" runat="server" ErrorMessage="Select Image!"></asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="form-group">
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <asp:Button ID="btnAdd" CssClass="btn btn-default" runat="server" ValidationGroup="error" Text="Add" OnClick="btnAdd_Click" />
                    <asp:Label ID="lblerror" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <h1>Categories</h1>
        <hr />

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">All Categories</div>
            <asp:Repeater ID="rptrCat" runat="server">
                <HeaderTemplate>
                    <table class="table lblcol">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Category</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <th><%# Eval("CatID") %></th>
                        <td><%# Eval("CatName") %></td>
                        <td>Edit</td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                 </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>

