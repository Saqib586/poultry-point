﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignIn.aspx.cs" Inherits="SignIn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sign In</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/Custom-Cs.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
   
</head>
<body>
    <div style="background: url('Images/beach-dawn-dusk-292442.jpg') no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    height:100%;">
    
    <form id="form1" runat="server">
    <div>
        
        <!-- Navigation -->

    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><span>
                            <img alt="logo" src="Images/logo.jpg" height="30" /></span>Poultry Point</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="Default.aspx">Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Products.aspx">All Products</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="Categories.aspx">Categories</a></li>
                                </ul>
                            </li>
                            <li><a href="Diseases.aspx">Diseases</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Mission.aspx">Mission</a></li>
                                    <li role="separator" class="divider"></li>
                                <li><a href="Vision.aspx">Vision</a></li>
                                </ul>
                            </li>
                            <li><a href="ContactUs.aspx">Contact Us</a></li>
                            
                            <li class="active"><a href="SignIn.aspx">Sign In</a></li>
                            </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Navigation -->

        <!-- Sign In -->
        <div class="container">
            <div class="form-horizontal">
                
                <h2>Login</h2>
                <div class="divDet3"></div>
                <div class="form-group">
                    <asp:Label ID="Label1" CssClass="col-md-2 control-label " runat="server" Text="Username"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox ID="UserName" CssClass="form-control" placeholder="Username" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorUsername" CssClass="text-danger" ControlToValidate="UserName" runat="server" ErrorMessage="Username Field is Empty"></asp:RequiredFieldValidator>
                </div>
                </div>

                <div class="form-group">
                    <asp:Label ID="Label2" CssClass="col-md-2 control-label " runat="server" Text="Password"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox ID="Password" CssClass="form-control" placeholder="Password" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorPass" CssClass="text-danger" ControlToValidate="Password"  runat="server" ErrorMessage="Password Field is Empty"></asp:RequiredFieldValidator>
                </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                         <asp:CheckBox ID="CheckBox1" runat="server" />
                    <asp:Label ID="Label3" CssClass=" control-label " runat="server" Text="Remember me ?"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2"> </div>
                        <div class="col-md-6">
                            <asp:Button ID="Button1" CssClass="mainButton" runat="server" Text="Login" OnClick="Button1_Click" />
                            
                    </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-6">
                            <asp:LinkButton ID="lbforpass" CssClass="buyNowBtn" runat="server" PostBackUrl="~/ForgotPassword.aspx">Forgot Password !</asp:LinkButton>
                        <asp:LinkButton ID="LinkButton1" CssClass="buyNowBtn" runat="server" PostBackUrl="~/SignUp.aspx">Sign Up</asp:LinkButton>
                        </div>
                    </div>
                
            <div class="form-group">
                <div class="col-md-2"> </div>
                        <div class="col-md-6">
                            <asp:Label ID="lblError" runat="server" CssClass="text-danger"></asp:Label>
                             </div>
            </div>
            
            </div>
                
            </div>

        <!-- Sign In -->

        <div class="divDet3"></div>
        <div class="divDet3"></div>
        <div class="divDet3"></div>
        <div class="divDet3"></div>
        <div class="divDet3"></div>
        <div class="divDet3"></div>
         <div class="divDet3"></div>
        <div class="divDet3"></div>
        <div class="divDet3"></div>

    </form>
     <!-- Footer -->
       

        <footer class="panel-footer navbar-fixed-bottom">
            <div class="container lblcol">
            <p class="pull-right"><a href="#"> Back on top</a> </p>
                <p>Poultry Point&trade; 2018 &middot; <a href="Default.aspx">Home</a> &middot; <a href="#">About Us</a> &middot; <a href="ContactUs.aspx">Contact Us</a> &middot; <a href="Products.aspx">Shop</a> &middot;</p>
        </div>
                </footer>

        <!-- Footer -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
       </div>
</body>
</html>
