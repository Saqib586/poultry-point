﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Poultry Point</title>
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/Custom-Cs.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        $(document).ready(function myfunction() {
            $("#btnCart").click(function myfunction() {
                window.location.href = "/Cart.aspx";
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <!-- Navigation -->

            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><span>
                            <img class="img-rounded" alt="logo" src="Images/logo.jpg" height="30" /></span>Poultry Point</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="Default.aspx">Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Products.aspx">All Products</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="Categories.aspx">Categories</a></li>

                                </ul>

                            </li>

                            <li><a href="Diseases.aspx">Diseases</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Mission.aspx">Mission</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="Vision.aspx">Vision</a></li>
                                </ul>
                            </li>
                            <li><a href="ContactUs.aspx">Contact Us</a></li>
                            <li>
                                <button id="btnCart" class="btn btn-primary navbar-btn" type="button">
                                    Cart <span class="badge" id="pCount" runat="server"></span>
                                </button>
                            </li>
                            <li id="btnSignUp" runat="server"><a href="SignUp.aspx">Sign Up</a></li>
                            <li id="btnSignIn" runat="server"><a href="SignIn.aspx">Sign In</a></li>
                            <li>
                                <asp:Button ID="btnSignOut" Class="btn btn-danger navbar-btn" runat="server" Text="Sign Out" OnClick="btnSignOut_Click" />
                            <div class="navbar-form navbar-left">
                          <input type="text" class="form-control" id="searchtextbox" placeholder="Product Name" />
                               
                                <input type="button" class="btn btn-success" value="Search" onclick="search();" />
                                    
                                
                            </div>
                            </li>
                            
                        </ul>
                        <div class="Dev3"></div>
                                <div class="Dev3"></div>
                                <div class="Dev3"></div>
                        
                    </div>
                </div>
            </div>
           <%-- <div class="container center-page">
                <asp:GridView ID="gvsearch" runat="server"></asp:GridView>
            </div>--%>
            <!-- Navigation -->
           
            <!-- Carousel -->
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="6" class="active"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item ">
                        <img src="Images/carousel 1.jpg" alt="carousel 1" />
                        <div class="carousel-caption">
                            <p><a class="btn btn-lg btn-primary" href="SignUp.aspx" role="button">Join Us Today</a></p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="Images/carousel 2.jpg" alt="carousel 2" />
                        <div class="carousel-caption">
                            <p><a class="btn btn-lg btn-primary" href="SignUp.aspx" role="button">Join Us Today</a></p>

                        </div>
                    </div>
                    <div class="item">
                        <img src="Images/carousel 3.jpg" alt="carousel 3" />
                        <div class="carousel-caption">
                            <p><a class="btn btn-lg btn-primary" href="SignUp.aspx" role="button">Join Us Today</a></p>

                        </div>
                    </div>
                    <div class="item">
                        <img src="Images/carousel 4.jpg" alt="carousel 4" />
                        <div class="carousel-caption">
                            <p><a class="btn btn-lg btn-primary" href="SignUp.aspx" role="button">Join Us Today</a></p>

                        </div>
                    </div>
                    <div class="item ">
                        <img src="Images/carousel 5.jpg" alt="carousel 5" />
                        <div class="carousel-caption">
                            <p><a class="btn btn-lg btn-primary" href="SignUp.aspx" role="button">Join Us Today</a></p>

                        </div>
                    </div>
                    <div class="item">
                        <img src="Images/carousel 6.jpg" alt="carousel 6" />
                        <div class="carousel-caption">
                            <p><a class="btn btn-lg btn-primary" href="SignUp.aspx" role="button">Join Us Today</a></p>
                        </div>


                    </div>
                    <div class="item active">
                        <img src="Images/carousel 7.jpg" alt="carousel 7" />
                        <div class="carousel-caption">
                            <p><a class="btn btn-lg btn-primary" href="SignUp.aspx" role="button">Join Us Today</a></p>
                        </div>


                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- Carousel -->
        </div>
        <br />
        <br />
        <!-- Middle contents -->
        <div class="container center">
            <div class="row">
                <div class="col-lg-4">
                    <img class="img-circle" src="Images/mid1.jpg" alt="mid1" width="140" height="140" />
                    <h2>Coops</h2>
                    <p>A chicken coop or hen house is a small house where, typically, female chickens or other fowl are kept safe and secure. There are nest boxes found inside the hen houses for egg-laying, and perches on which the birds can sleep. A chicken coop usually has an indoor area where the chickens can sleep and nest, as well as a fenced-in outdoor area.</p>
                    <p><a class="btn btn-default" href="ProductView.aspx?PID=37" role="button">View &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <img class="img-circle" src="Images/mid2.jpeg" alt="mid2" width="140" height="140" />
                    <h2>Vaccines</h2>
                    <p>Vaccination plays an important part in the health management of the poultry flock. There are numerous diseases that are prevented by vaccinating the birds against them. A vaccine helps to prevent a particular disease by triggering or boosting the bird’s immune system to produce antibodies that in turn fight the invading causal organisms.</p>
                    <p><a class="btn btn-default" href="ProductView.aspx?PID=42" role="button">View &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <img class="img-circle" src="Images/mid3.jpg" alt="mid3" width="140" height="140" />
                    <h2>Chicken Breed</h2>
                    <p>There are hundreds of chicken breeds in existence. Domesticated for thousands of years, distinguishable breeds of chicken have been present since the combined factors of geographical isolation and selection for desired characteristics created regional types with distinct physical and behavioral traits passed on to their offspring. Chickens lay over 200 eggs a year.</p>
                    <p><a class="btn btn-default" href="ProductView.aspx?PID=38" role="button">View &raquo;</a></p>
                </div>
            </div>
        </div>
        <!-- Middle contents -->
        <div class="divDet2"></div>
        <div class="divDet2"></div>
        <div class="divDet2"></div>
        <!-- Footer -->
        <hr />

        <footer class="panel-footer navbar-fixed-bottom">
            <div class="container lblcol">
                <p class="pull-right"><a href="#">Back on top</a> </p>
                <p>Poultry Point&trade; 2018 &middot; <a href="Default.aspx">Home</a> &middot; <a href="#">About Us</a> &middot; <a href="ContactUs.aspx">Contact Us</a> &middot; <a href="Products.aspx">Shop</a> &middot;</p>
            </div>
        </footer>
        <!-- Footer -->

    </form>
    <script type="text/javascript">
        function search() {
            window.location = "Products.aspx?search=" + document.getElementById("searchtextbox").value;
        }
    </script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>


</body>
</html>
