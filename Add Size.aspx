﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="Add Size.aspx.cs" Inherits="Add_Size" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container">
        <div class="form-horizontal">
            <h2>Add Size</h2>
            <hr />
            
             <div class="form-group">
                    <asp:Label ID="Label2" CssClass="col-md-2 control-label " runat="server" Text="Size Name"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox ID="txtSizeName" CssClass="form-control" runat="server" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="text-danger" ValidationGroup="error" ControlToValidate="txtSizeName" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label3" CssClass="col-md-2 control-label " runat="server" Text="Category"></asp:Label>
                <div class="col-md-3">
                    <asp:DropDownList ID="ddlCategory" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="text-danger" ValidationGroup="error" ControlToValidate="ddlCategory" runat="server" InitialValue="0" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label4" CssClass="col-md-2 control-label " runat="server" Text="Sub Category"></asp:Label>
                <div class="col-md-3">
                    <asp:DropDownList ID="ddlSubCategory" CssClass="form-control"  runat="server"></asp:DropDownList>
                      <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="text-danger"  ValidationGroup="error" ControlToValidate="ddlSubCategory" runat="server" InitialValue="0" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="form-group">
                    <div class="col-md-2"> </div>
                        <div class="col-md-6">
                            <asp:Button ID="btnAdd" CssClass="btn btn-default" runat="server" Text="Add" ValidationGroup="error" OnClick="btnAdd_Click"   />
                            <asp:Label ID="lblerror" runat="server" ></asp:Label>
                            </div>
                
                </div>
            </div>
        <h1>Sizes</h1>
        <hr />

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">All Sizes</div>
            <asp:Repeater ID="rptrCat" runat="server">
                <HeaderTemplate>
                    <table class="table lblcol">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Sub Category</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <th><%# Eval("SizeID") %></th>
                        <td><%# Eval("SizeName") %></td>
                        <td><%# Eval("CatName") %></td>
                        <td><%# Eval("SubCatName") %></td>
                        <td>Edit</td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                 </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
         </div>
</asp:Content>

