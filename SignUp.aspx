﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignUp.aspx.cs" Inherits="SignUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sign Up</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/Custom-Cs.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
     <div style="background: url('Images/beach-dawn-dusk-292442.jpg') no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    height:100%;">
    <form id="form1" runat="server">
        <div>
     <!-- Navigation -->
          <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><span>
                            <img alt="logo" src="Images/logo.jpg" height="30" /></span>Poultry Point</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="Default.aspx">Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Products.aspx">All Products</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="Categories.aspx">Categories</a></li>
                                </ul>
                            </li>
                            <li><a href="Diseases.aspx">Diseases</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Mission.aspx">Mission</a></li>
                                    <li role="separator" class="divider"></li>
                                <li><a href="Vision.aspx">Vision</a></li>
                                </ul>
                            </li>
                            <li><a href="ContactUs.aspx">Contact Us</a></li>
                            <li class="active"><a href="SignUp.aspx">Sign Up</a></li>
                            <li><a href="SignIn.aspx">Sign In</a></li>
                            </ul>
                    </div>
                </div>
            </div>
       </div>
        <!-- Navigation -->

        <!-- Sign Up -->
        <div class="container">
            <div class="form-horizontal">
                <h2>SignUp</h2>
                <div class="divDet3"></div>

                <div class="form-group">
                    <asp:Label ID="lblun" CssClass="col-md-2 control-label " runat="server" Text="Username"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="tbUname" runat="server" Class="form-control" Placeholder="Usename"></asp:TextBox>
            </div>
                    </div>
                <div class="form-group">
                    <asp:Label ID="lblp" CssClass="col-md-2 control-label " runat="server" Text="Password"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="tbPass" runat="server" Class="form-control" Placeholder="Password" TextMode="Password"></asp:TextBox>
            </div>
                    </div>

             <div class="form-group">
                    <asp:Label ID="lblcp" CssClass="col-md-2 control-label " runat="server" Text="Confirm Password"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="tbCPass" runat="server" Class="form-control" Placeholder="Confirm Password" TextMode="Password"></asp:TextBox>
            </div>
                 </div>

            <div class="form-group">
                    <asp:Label ID="lblnam" CssClass="col-md-2 control-label " runat="server" Text="Name"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="tbName" runat="server" Class="form-control" Placeholder="Name"></asp:TextBox>
            </div>
                </div>

            <div class="form-group">
                    <asp:Label ID="lblem" CssClass="col-md-2 control-label " runat="server" Text="Email"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="tbEmail" runat="server" Class="form-control" Placeholder="Email" TextMode="Email"></asp:TextBox>
            </div>
                </div>

            <div class="form-group">
                    <div class="col-md-2"> </div>
                        <div class="col-md-6">
                <asp:Button ID="btSignup" runat="server" Class="btn btn-success" Text="Sign Up" OnClick="btSignup_Click" />
                <asp:Label ID="lblMsg" runat="server"></asp:Label>
            </div>
                </div>
                </div>
        </div>
        <!-- Sign Up -->
        </form>
          <div class="divDet3"></div>
        <div class="divDet3"></div>
        <div class="divDet3"></div>
        <div class="divDet3"></div>
        <div class="divDet3"></div>
        <div class="divDet3"></div>
         <div class="divDet3"></div>
        <div class="divDet3"></div>
        <div class="divDet3"></div>
           
          <!-- Footer -->
           

            <footer class="panel-footer navbar-fixed-bottom">
                <div class="container lblcol">
                    <p class="pull-right"><a href="#">Back on top</a> </p>
                <p>Poultry Point&trade; 2018 &middot; <a href="Default.aspx">Home</a> &middot; <a href="#">About Us</a> &middot; <a href="ContactUs.aspx">Contact Us</a> &middot; <a href="Products.aspx">Shop</a> &middot;</p>
                </div>
            </footer>
            <!-- Footer -->

      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="js/jquery-3.3.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
         </div>
</body>
</html>
