﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

public partial class Add_SubCategory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMainCategory();
            BindCatRptr();
        }
    }

    private void BindCatRptr()
    {
        String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        using (SqlConnection con = new SqlConnection(CS))
        {
            using (SqlCommand cmd = new SqlCommand("select A.*,B.* from tblSubCategories A inner join tblCategories B on B.CatID=A.MainCatID", con))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                {
                    DataTable dtCat = new DataTable();
                    sda.Fill(dtCat);
                    rptrCat.DataSource = dtCat;
                    rptrCat.DataBind();


                }
            }
        }
    }

    private void BindMainCategory()
    {
        String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("select * from tblCategories", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            if (dt.Rows.Count != 0)
            {
                ddlCategory.DataSource = dt;
                ddlCategory.DataTextField = "CatName";
                ddlCategory.DataValueField = "CatID";
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("-Select-", "0"));
            }

        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
            try
            {
                String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand("insert into tblSubCategories values('" + txtSubCatName.Text + "', '" + ddlCategory.SelectedItem.Value + "')", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    txtSubCatName.Text = string.Empty;
                    ddlCategory.ClearSelection();
                    ddlCategory.Items.FindByValue("0").Selected = true;
                    lblerror.Text = "Data Added Successfully";
                    lblerror.ForeColor = System.Drawing.Color.Green;
                }
                BindCatRptr();
            }
            catch (Exception ex)
            {
                lblerror.Text = (ex.Message);
                lblerror.ForeColor = System.Drawing.Color.Red;
            }
        }
    }