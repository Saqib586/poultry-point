﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminHome : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnProductAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Add Products.aspx");
    }
    protected void btnCatadd_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Add Category.aspx");
    }
    protected void btnSubCatadd_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Add SubCategory.aspx");
    }
    protected void btnSizeadd_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Add Size.aspx");
    }
}