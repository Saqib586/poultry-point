﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="ForgotPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Forgot Password</title>
     <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/Custom-Cs.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="sweetalert.js" type="text/javascript"></script>
    <script>
        function wait() {

            swal({
                
                title: 'Password Request Sent, Check Email',
                type: 'success',
                //timer: 3000,
                showConfirmButton: true
            })
        }
    </script>
    <script>
        function Showalert() {
            swal({

                ////title: 'Password Request Sent, Check Email',
                //type: 'success',
                //timer: 3000,
                //showConfirmButton: false
            })
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <!--NAVBAR-->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><span>
                            <img alt="logo" src="Images/logo.jpg" height="30" /></span>Poultry Point</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="Default.aspx">Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Products.aspx">All Products</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="Categories.aspx">Categories</a></li>
                                </ul>
                            </li>
                            <li><a href="Diseases.aspx">Diseases</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Mission.aspx">Mission</a></li>
                                    <li role="separator" class="divider"></li>
                                <li><a href="Vision.aspx">Vision</a></li>
                                </ul>
                            </li>
                            <li><a href="ContactUs.aspx">Contact Us</a></li>
                            
                            <li><a href="SignIn.aspx">Sign In</a></li>
                            </ul>
                    </div>
                </div>
            </div>
    <!--NAVBAR-->
    </div>
        <div class="container">
            <div class="form-horizontal">
                <h3>Password Recovery</h3>
                <hr />
                <h4>Please enter your Email address, We will send you the reset password.</h4>
                <div class="form-group">
                    <asp:Label ID="lbEmail" runat="server" Text="Your Email" CssClass="col-md-2 control-label"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox ID="tbEmailid"  runat="server" CssClass="form-control" TextMode="Email"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmailid" runat="server" ErrorMessage="Please enter your Email ID !" CssClass="text-danger" ControlToValidate="tbEmailid"></asp:RequiredFieldValidator>
                    </div>                    
                </div>
                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                        <asp:Button ID="btnpassrec" runat="server" Text="Send" Class="btn btn-primary" OnClick="btnpassrec_Click" />
                        <asp:Label ID="lblPassReq" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </form>
<!-- Footer -->
        

        <footer class="panel-footer navbar-fixed-bottom">
            <div class="container lblcol">
            <p class="pull-right"><a href="#"> Back on top</a> </p>
                <p>Poultry Point&trade; 2018 &middot; <a href="Default.aspx">Home</a> &middot; <a href="#">About Us</a> &middot; <a href="ContactUs.aspx">Contact Us</a> &middot; <a href="Products.aspx">Shop</a> &middot;</p>
        </div>
                </footer>
        <!-- Footer -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
