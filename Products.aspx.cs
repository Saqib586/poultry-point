﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Products : System.Web.UI.Page
{
    String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        BindProductRepeater();

    }

    private void BindProductRepeater()
    {

        using (SqlConnection con = new SqlConnection(CS))
        {
            using (SqlCommand cmd = new SqlCommand("spsBindAllProducts", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                {
                    DataTable dtCat = new DataTable();
                    sda.Fill(dtCat);
                    rptrProducts.DataSource = dtCat;
                    rptrProducts.DataBind();
                }
            }

            if (Request.QueryString["search"] != null)
            {
                con.Open();
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from tblProducts P, tblProductImages I where P.PID=I.PID and PName like('%" + Request.QueryString["search"].ToString() + "%' ) ";

                cmd.ExecuteNonQuery();

                DataTable dtCat = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dtCat);
                rptrProducts.DataSource = dtCat;
                rptrProducts.DataBind();
            }
           
            }
        }
    }
