﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Add_Size : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindMainCategory();
            BindCatRptr();
        }
    }

    private void BindCatRptr()
    {
        try
        {
            String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                using (SqlCommand cmd = new SqlCommand("select A.*,B.*,D.* from tblSizes A inner join tblCategories B on B.CatID=A.CategoryID inner join tblSubCategories D on D.SubCatID=A.SubCategoryID", con))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        DataTable dtCat = new DataTable();
                        sda.Fill(dtCat);
                        rptrCat.DataSource = dtCat;
                        rptrCat.DataBind();


                    }
                }
            }
        }
        catch (Exception ex)
        {
            lblerror.Text = (ex.Message);
        }
    }
    private void BindMainCategory()
    {
        try
        {
            String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("select * from tblCategories", con);
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt.Rows.Count != 0)
                {
                    ddlCategory.DataSource = dt;
                    ddlCategory.DataTextField = "CatName";
                    ddlCategory.DataValueField = "CatID";
                    ddlCategory.DataBind();
                    ddlCategory.Items.Insert(0, new ListItem("-Select-", "0"));
                }

            }
        }
        catch (Exception ex)
        {
            lblerror.Text = (ex.Message);
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (txtSizeName.Text == "")
        {
            lblerror.Text = "All Fields Required";
            lblerror.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            try
            {
                String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand("insert into tblSizes values('" + txtSizeName.Text + "','" + ddlCategory.SelectedItem.Value + "','" + ddlSubCategory.SelectedItem.Value + "')", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    txtSizeName.Text = string.Empty;
                    ddlSubCategory.ClearSelection();
                    ddlSubCategory.Items.FindByValue("0").Selected = true;
                    ddlCategory.ClearSelection();
                    ddlCategory.Items.FindByValue("0").Selected = true;

                    lblerror.Text = "Data Added Successfully";
                    lblerror.ForeColor = System.Drawing.Color.Green;
                }
                BindCatRptr();
            }
            catch (Exception ex)
            {
                lblerror.Text = (ex.Message);
                lblerror.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
            int MainCategoryID = Convert.ToInt32(ddlCategory.SelectedItem.Value);

            String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("select * from tblSubCategories where MainCatID='" + ddlCategory.SelectedItem.Value + "'", con);
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt.Rows.Count != 0)
                {
                    ddlSubCategory.DataSource = dt;
                    ddlSubCategory.DataTextField = "SubCatName";
                    ddlSubCategory.DataValueField = "SubCatID";
                    ddlSubCategory.DataBind();
                    ddlSubCategory.Items.Insert(0, new ListItem("-Select-", "0"));
                }

            }
        }
     }