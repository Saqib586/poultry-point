﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserHome.aspx.cs" Inherits="UserHome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
   <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Welcome</title>
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/Custom-Cs.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
   <script>
       $(document).ready(function myfunction() {
           $("#btnCart").click(function myfunction() {
               window.location.href = "/Cart.aspx";
           });
       });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><span>
                            <img alt="logo" src="Images/logo.jpg" height="30" /></span>Poultry Point</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="Default.aspx">Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Products.aspx">All Products</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="Categories.aspx">Categories</a></li>
                                </ul>
                            </li>
                            <li><a href="Diseases.aspx"></a></li>
                             <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Mission.aspx">Mission</a></li>
                                    <li role="separator" class="divider"></li>
                                <li><a href="Vision.aspx">Vision</a></li>
                                </ul>
                            </li>
                            <li><a href="ContactUs.aspx">Contact Us</a></li>
                            <li>
                                <button id="btnCart" class="btn btn-primary navbar-btn" type="button">
                                    Cart <span class="badge" id="pCount" runat="server"></span>
                                </button>
                            </li>
                            <li>

                                <asp:Button ID="btnSignIn" Class="btn btn-default navbar-btn" runat="server" Text="Sign In" OnClick="btnSignIn_Click" />
                                <asp:Button ID="btnSignOut" Class="btn btn-default navbar-btn" runat="server" Text="Sign Out" OnClick="btnSignOut_Click" />
                            </li>
                           
                            </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Navigation -->
        <asp:Label ID="lblSuccess" runat="server" CssClass="text-success"></asp:Label>
    
    </form>
 <!-- Footer -->
        <hr />

        <footer class="panel-footer navbar-fixed-bottom">
            <div class="container">
            <p class="pull-right"><a href="#"> Back on top</a> </p>
                <p>Poultry Point&trade; 2018 &middot; <a href="Default.aspx">Home</a> &middot; <a href="#">About Us</a> &middot; <a href="ContactUs.aspx">Contact Us</a> &middot; <a href="Products.aspx">Shop</a> &middot;</p>
        </div>
                </footer>
        <!-- Footer -->

    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>