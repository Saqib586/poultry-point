﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.NetworkInformation;

public partial class Payment : System.Web.UI.Page
{
    bool connection = NetworkInterface.GetIsNetworkAvailable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["USERNAME"] != null)
        {
            if (!IsPostBack)
            {
                txtpnum.Text = "";
                txtName.Text = "";
                txtAddress.Text = "";
                BindPriceData();
            }
        }
        else
        {
            Response.Redirect("~/SignIn.aspx");
        }



    }

    public void BindPriceData()
    {
        if (Request.Cookies["CartPID"] != null)
        {

            string CookieData = Request.Cookies["CartPID"].Value.Split('=')[1];
            string[] CookieDataArray = CookieData.Split(',');
            if (CookieDataArray.Length > 0)
            {

                DataTable dtCat = new DataTable();

                Int64 CartTotal = 0;
                Int64 Total = 0;

                for (int i = 0; i < CookieDataArray.Length; i++)
                {
                    string PID = CookieDataArray[i].ToString().Split('-')[0];
                    string SizeID = CookieDataArray[i].ToString().Split('-')[1];

                    if (hdPidSizeID.Value != null && hdPidSizeID.Value != "")
                    {
                        hdPidSizeID.Value += "," + PID + "-" + SizeID;
                    }
                    else
                    {
                        hdPidSizeID.Value = PID + "-" + SizeID;
                    }
                    String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(CS))
                    {
                        using (SqlCommand cmd = new SqlCommand("select A.* ,dbo.getSizeName(" + SizeID + ") as SizeNamee, "
                            + SizeID + " as SizeIDD,SizeData.Name,SizeData.Extension from tblProducts A cross apply(select top 1 B.Name,Extension from tblProductImages B where B.PID=A.PID) SizeData where A.PID="
                            + PID + "", con))
                        {
                            cmd.CommandType = CommandType.Text;
                            using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                            {

                                sda.Fill(dtCat);
                            }
                        }
                    }
                    CartTotal += Convert.ToInt64(dtCat.Rows[i]["PPrice"]);
                    Total += Convert.ToInt64(dtCat.Rows[i]["PSelPrice"]);

                }

                BuyNowPriceDetails.Visible = true;

                spanCartTotal.InnerText = CartTotal.ToString();
                spanTotal.InnerText = "Rs. " + Total.ToString();
                spanDiscount.InnerText = "- " + (CartTotal - Total).ToString();

                hdCartAmount.Value = CartTotal.ToString();
                hdCartDiscount.Value = (CartTotal - Total).ToString();
                hdTotalPayed.Value = Total.ToString();
            }
            else
            {
                //show empty cart
                Response.Redirect("~/Products.aspx");

            }
        }
        else
        {
            //show empty cart
            Response.Redirect("~/Products.aspx");
        }

    }
    protected void btnEasyPaisa_Click(object sender, EventArgs e)
    {
        string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(CS))
        {
            try
            {
                conn.Open();
                SqlCommand sc = new SqlCommand("dataforpopup", conn);
                int userid = int.Parse(Session["USERID"].ToString());

                sc.Parameters.AddWithValue("userid", userid);

                sc.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sda = new SqlDataAdapter(sc);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {

                    span3.InnerText = ds.Tables[0].Rows[0][0].ToString();
                    span4.InnerText = ds.Tables[0].Rows[0][1].ToString();

                    ModalPopupExtender2.Show();


                }

            }
            catch (Exception exp)
            {
                Response.Write(exp.Message);
            }




        }

    }
    public void Button2_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Hide();
    }
    protected void btnjcash_Click(object sender, EventArgs e)
    {
        string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        using (SqlConnection conn = new SqlConnection(CS))
        {
            try
            {
                conn.Open();
                SqlCommand sc = new SqlCommand("dataforpopup", conn);
                int userid = int.Parse(Session["USERID"].ToString());

                sc.Parameters.AddWithValue("userid", userid);

                sc.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sda = new SqlDataAdapter(sc);
                DataSet ds = new DataSet();
                sda.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {

                    span2.InnerText = ds.Tables[0].Rows[0][0].ToString();
                    span1.InnerText = ds.Tables[0].Rows[0][1].ToString();

                    ModalPopupExtender1.Show();
                   

                }

            }
            catch (Exception exp)
            {
                Response.Write(exp.Message);
            }




        }

    }
    protected void btnCOD_Click(object sender, EventArgs e)
    {
        if (connection == true)
        {

            if (Session["USERID"] != null)
            {
                string USERID = Session["USERID"].ToString();
                SENDCONFIRMATION obj = new SENDCONFIRMATION();
                obj.SendMessage(int.Parse(USERID));
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "cod", "shipped()", true);
                btnCOD.Enabled = false;
                btnjcash.Enabled = false;
                btnEasyPaisa.Enabled = false;


            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "cod", "error()", true);
        }
    }
    
    protected void btncan_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
    }
    protected void sendcusvalue_Click(object sender, EventArgs e)
    {
        if (Session["USERID"] != null)
        {
            string USERID = Session["USERID"].ToString();
            string PaymentType = "EasyPaisa";
            string PaymentStatus = "notPaid";
            DateTime DateOfPurchase = DateTime.Now;


            //Database of Table Purchase

            String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("insert into tblPurchase values('" + USERID + "','"
                    + hdPidSizeID.Value + "','" + hdCartAmount.Value + "','" + hdCartDiscount.Value + "','"
                    + hdTotalPayed.Value + "','" + PaymentType + "','" + PaymentStatus + "','"
                    + DateOfPurchase + "','" + txtName.Text + "','" + txtAddress.Text + "','"
                    + txtpnum.Text + "')select SCOPE_IDENTITY()", con);
                con.Open();
                Int64 PurchaseID = Convert.ToInt64(cmd.ExecuteScalar());
                sendcusvalue.Enabled = false;
                btnCOD.Enabled = true;
                btnEasyPaisa.Enabled = true;
                btnjcash.Enabled = true;
                txtAddress.Enabled = false;
                txtName.Enabled = false;
                txtpnum.Enabled = false;
                //SENDCONFIRMATION obj = new SENDCONFIRMATION();
                //obj.SendMessage(int.Parse(USERID));
                ScriptManager.RegisterClientScriptBlock(this,GetType(),"ok","alertme()",true);

                // ClientScript.RegisterStartupScript(this, GetType(), "myfunction", "myfunction();", true);

            }
        }
        else
        {
            Response.Redirect("~/SignIn.aspx");
        }
    }
    protected void tsub_Click(object sender, EventArgs e)
    {
        if (connection == true)
        {
            if (Session["USERID"] != null)
            {
                string USERID = Session["USERID"].ToString();
                SENDCONFIRMATION obj = new SENDCONFIRMATION();
                obj.SendMessage(int.Parse(USERID));
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "cod", "transaction()", true);
                ModalPopupExtender1.Hide();
                btnCOD.Enabled = false;
                btnjcash.Enabled = false;
                btnEasyPaisa.Enabled = false;

            }
        }
        else { ScriptManager.RegisterClientScriptBlock(this, GetType(), "cod", "error()", true); }
    }
    protected void submitEP_Click(object sender, EventArgs e)
    {
        if (connection == true)
        {
            if (Session["USERID"] != null)
            {
                string USERID = Session["USERID"].ToString();
                SENDCONFIRMATION obj = new SENDCONFIRMATION();
                obj.SendMessage(int.Parse(USERID));
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "cod", "transaction()", true);
                ModalPopupExtender1.Hide();
                btnCOD.Enabled = false;
                btnjcash.Enabled = false;
                btnEasyPaisa.Enabled = false;

            }
        }
        else { ScriptManager.RegisterClientScriptBlock(this, GetType(), "cod", "error()", true); }
    }
}

