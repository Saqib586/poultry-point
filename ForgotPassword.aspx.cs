﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Net.Mail;
using System.Net;

public partial class ForgotPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnpassrec_Click(object sender, EventArgs e)
    {
        
        if (tbEmailid.Text == "")
        {
            
        }
        else
        {
           
                String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
                using (SqlConnection con = new SqlConnection(CS))
                {
                     try
            {
                    SqlCommand cmd = new SqlCommand("select * from Users where Email='" + tbEmailid.Text + "'", con);
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    if (dt.Rows.Count != 0)
                    {
                        String myGUID = Guid.NewGuid().ToString();
                        int Uid = Convert.ToInt32(dt.Rows[0][0]);
                        SqlCommand cmd1 = new SqlCommand("insert into ForgotPassRequests values('" + myGUID + "','" + Uid + "',getdate())", con);
                        cmd1.ExecuteNonQuery();

                        //Email reset code here
                        String ToEmailAddress = dt.Rows[0][3].ToString();
                        String Username = dt.Rows[0][1].ToString();
                        String EmailBody = "Hi " + Username + ",<br/><br/> Click the link below to reset your Password ! <br/> <br/> http://localhost:54427/RecoverPassword.aspx?Uid=" + myGUID;
                        MailMessage PassReqMail = new MailMessage("youremail@gmail.com", ToEmailAddress);

                        PassReqMail.Body = EmailBody;
                        PassReqMail.IsBodyHtml = true;
                        PassReqMail.Subject = "Reset Password";

                        //SMTP Code
                        SmtpClient SMTP = new SmtpClient("smtp.gmail.com", 587);
                        SMTP.Credentials = new NetworkCredential()
                        {
                            UserName = "poultrypoint2018@gmail.com",
                            Password = "03365350966"
                        };

                        SMTP.EnableSsl = true;
                        SMTP.Send(PassReqMail);
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "cod", "wait()", true);
                        //lblPassReq.Text = "Check your Email to reset password.";
                        //lblPassReq.ForeColor = Color.Green;
                        tbEmailid.Text = "";
                    }
                    else
                    {
                        lblPassReq.Text = "Oops! The Email you entered is not available in our Database.";
                        lblPassReq.ForeColor = Color.Red;
                    }
                }
                     catch (Exception exc)
                     {
                         ScriptManager.RegisterClientScriptBlock(this, GetType(), "showalert", "swal('" + exc.Message.ToString() + "');", true);
                     }
            }
            
        }
    }
}