﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="Add Products.aspx.cs" Inherits="Add_Products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="container">
        <div class="form-horizontal">
            <h2>Add Product</h2>
            <hr />
            <div class="form-group">
                    <asp:Label ID="Label1" CssClass="col-md-2 control-label " runat="server" Text="Product Name"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox ID="txtPName" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorProName" CssClass="text-danger" ControlToValidate="txtPName" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
             <div class="form-group">
                    <asp:Label ID="Label2" CssClass="col-md-2 control-label " runat="server" Text="Price"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox ID="txtPrice" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="text-danger" ControlToValidate="txtPrice" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
             <div class="form-group">
                    <asp:Label ID="Label3" CssClass="col-md-2 control-label " runat="server" Text="Selling Price"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox ID="txtSelPrice" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="text-danger" ControlToValidate="txtSelPrice" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label4" CssClass="col-md-2 control-label " runat="server" Text="Category"></asp:Label>
                <div class="col-md-3">
                    <asp:DropDownList ID="ddlCategory" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="text-danger" InitialValue="0" ControlToValidate="ddlCategory" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label5" CssClass="col-md-2 control-label " runat="server" Text="Sub Category"></asp:Label>
                <div class="col-md-3">
                    <asp:DropDownList ID="ddlSubCategory" OnSelectedIndexChanged="ddlSubCategory_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server"></asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" CssClass="text-danger" InitialValue="0" ControlToValidate="ddlSubCategory" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
             <div class="form-group">
                    <asp:Label ID="Label6" CssClass="col-md-2 control-label " runat="server" Text="Size"></asp:Label>
                <div class="col-md-3">
                    <asp:CheckBoxList ID="cblSize" RepeatDirection="Horizontal" CssClass="form-control" runat="server"></asp:CheckBoxList>

                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label18" CssClass="col-md-2 control-label " runat="server" Text="Quantity"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox ID="txtQuantity" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" CssClass="text-danger" ControlToValidate="txtQuantity" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label7" CssClass="col-md-2 control-label " runat="server" Text="Description"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox ID="txtDesc" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" CssClass="text-danger" ControlToValidate="txtDesc" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label8" CssClass="col-md-2 control-label " runat="server" Text="Product Details"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox ID="txtPDetails" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" CssClass="text-danger" ControlToValidate="txtPDetails" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label9" CssClass="col-md-2 control-label " runat="server" Text="Material and Care"></asp:Label>
                <div class="col-md-3">
                    <asp:TextBox ID="txtMatCare" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" CssClass="text-danger" ControlToValidate="txtMatCare" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
             <div class="form-group">
                    <asp:Label ID="Label10" CssClass="col-md-2 control-label " runat="server" Text="Upload Image"></asp:Label>
                <div class="col-md-3">
                    <asp:FileUpload ID="fuImg01" runat="server" CssClass="form-control" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" CssClass="text-danger" ControlToValidate="fuImg01" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label11" CssClass="col-md-2 control-label " runat="server" Text="Upload Image"></asp:Label>
                <div class="col-md-3">
                    <asp:FileUpload ID="fuImg02" runat="server" CssClass="form-control" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" CssClass="text-danger" ControlToValidate="fuImg02" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label12" CssClass="col-md-2 control-label " runat="server" Text="Upload Image"></asp:Label>
                <div class="col-md-3">
                    <asp:FileUpload ID="fuImg03" runat="server" CssClass="form-control" />
<%--                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" CssClass="text-danger" ControlToValidate="fuImg03" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>--%>
                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label13" CssClass="col-md-2 control-label " runat="server" Text="Upload Image"></asp:Label>
                <div class="col-md-3">
                    <asp:FileUpload ID="fuImg04" runat="server" CssClass="form-control" />
<%--                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" CssClass="text-danger" ControlToValidate="fuImg04" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>--%>
                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label14" CssClass="col-md-2 control-label " runat="server" Text="Upload Image"></asp:Label>
                <div class="col-md-3">
                    <asp:FileUpload ID="fuImg05" runat="server" CssClass="form-control" />
<%--                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" CssClass="text-danger" ControlToValidate="fuImg05" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>--%>
                </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label15" CssClass="col-md-2 control-label " runat="server" Text="Free Delivery"></asp:Label>
                <div class="col-md-3">
                    <asp:CheckBox ID="cbFD" runat="server" />
                    </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label16" CssClass="col-md-2 control-label " runat="server" Text="30 Days Return"></asp:Label>
                <div class="col-md-3">
                    <asp:CheckBox ID="cb30Ret" runat="server" />
                    </div>
                </div>
            <div class="form-group">
                    <asp:Label ID="Label17" CssClass="col-md-2 control-label " runat="server" Text="COD"></asp:Label>
                <div class="col-md-3">
                    <asp:CheckBox ID="cbCOD" runat="server" />
                    </div>
                </div>
            <div class="form-group">
                    <div class="col-md-2"> </div>
                        <div class="col-md-6">
                            <asp:Button ID="btnAdd" CssClass="btn btn-default" runat="server" Text="Add" OnClick="btnAdd_Click" />
                            <asp:Label ID="lblerror" runat="server"></asp:Label>
                    </div>
                    </div>
        </div>
    </div>
</asp:Content>

