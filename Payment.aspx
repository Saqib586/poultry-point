﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="Payment.aspx.cs" Inherits="Payment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="sweetalert.js" type="text/javascript"></script>
    <script>
        function alertme() {
            swal({
                title: 'Choose Payment Method!',
                type: 'info',
                timer: 3000,
                showConfirmButton: false
            })
        }
    </script>
    <script>
        function shipped() {
            swal({
                title: 'Done',
                title: 'Your Package has been Delivered',
                type: 'success',
                timer: 3000,
                showConfirmButton: false
            })
        }
    </script>

    <script>
        function transaction() {
            swal({
                title: 'Transaction Successful',
                text: 'Your Package has been Delivered',
                type: 'success',
                timer: 3000,
                showConfirmButton: false
            })
        }
    </script>
    <script>function error() {
    swal({
        type: 'error',
        title: 'Oops...',
        text: 'Check Internet Connectivity!'

    })
}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:HiddenField ID="hdCartAmount" runat="server" />
    <asp:HiddenField ID="hdCartDiscount" runat="server" />
    <asp:HiddenField ID="hdTotalPayed" runat="server" />
    <asp:HiddenField ID="hdPidSizeID" runat="server" />

    <div style="padding-top: 20px;">
        <div class="col-md-9">
            <div class="form-horizontal">
                <h3>Delivery Address</h3>
                <hr />
                <div class="form-group">
                    <asp:Label ID="Label1" CssClass="col-md-2 control-label " runat="server" Text="Your Name"></asp:Label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtName" CssClass="form-control" runat="server" Enabled="true"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorProName" CssClass="text-danger" ControlToValidate="txtName" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label2" CssClass="col-md-2 control-label " runat="server" Text="Your Address"></asp:Label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtAddress" TextMode="MultiLine" CssClass="form-control" runat="server" Enabled="true"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="text-danger" ControlToValidate="txtAddress" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label ID="Label3" CssClass="col-md-2 control-label " runat="server" Text="Mobile Number" Enabled="true"></asp:Label>
                    <div class="col-md-7">
                        <asp:TextBox ID="txtpnum" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="text-danger" ControlToValidate="txtpnum" runat="server" ErrorMessage="This Field is Required !"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3" runat="server" id="BuyNowPriceDetails">
            <div style="border-bottom: 1px solid #eaeaec;">
                <h5 class="proNameViewCart">PRICE DETAILS</h5>
                <div>
                    <label>Cart Total</label>
                    <span class="pull-right carttotalprice" id="spanCartTotal" runat="server"></span>
                </div>
                <div>
                    <label>Cart Discount</label>
                    <span class="pull-right priceGreen" id="spanDiscount" runat="server"></span>
                </div>
            </div>
            <div class="proPriceView">
                <label>Total</label>
                <span class="pull-right" id="spanTotal" runat="server"></span>
            </div>
        </div>
        <asp:Button ID="sendcusvalue" runat="server" Text="Next" CssClass="buyNowBtn" Style="font-size: larger" OnClick="sendcusvalue_Click" Enabled="true" />
        <div class="col-md-12">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <h3>Choose Payment Method</h3>
            <hr />
            <ul class="nav nav-tabs">
                <li><a data-toggle="tab" href="#cod">
                    <img class="img-circle" alt="plogo" src="Images/Payment Logo/images (1).png" height="40" /><strong>Cash On Delivery</strong></a></li>
                <li class="active"><a data-toggle="tab" id="jcash" runat="server" href="#paypal">
                    <img class="img-circle" alt="plogo" src="Images/Payment Logo/Jazz-and-Western-Union.jpg" height="40" /><strong>Jazz Cash</strong></a></li>
                <li><a data-toggle="tab" href="#wallets">
                    <img class="img-circle" alt="plogo" src="Images/Payment Logo/images.png" height="40" /><strong>EasyPaisa</strong></a></li>
            </ul>
            <div class="tab-content">

                <div id="cod" class="tab-pane fade">
                    <h3>Cash On Delivery</h3>
                    <p>
                        Pay in cash to the courier at the time of order delivery.<br />
                        <br />
                        <strong>You’re almost done!</strong><br />
                        To change or edit your order, go back. No changes will be allowed once you click "CONFIRM ORDER".
                    </p>

                    <asp:Button ID="btnCOD" CssClass="buyNowBtn" runat="server" Text="CONFIRM ORDER" Width="150" OnClick="btnCOD_Click" Enabled="false" />
                </div>

                <div id="paypal" class="tab-pane fade in active">
                    <h3 class="jazzcolor">Jazz Cash</h3>
                    <p>
                        JazzCash Mobile Account can be registered on any Jazz or Warid number.<br />
                        <br />
                        Jazz and Warid customers can self-register their Mobile Account simply by dialing *786#.<br />
                        <br />
                        <strong>You’re almost done!</strong><br />
                        To change or edit your order, go back. No changes will be allowed once you click "CONFIRM ORDER".
                    </p>
                    <asp:Button ID="btnjcash" CssClass="jazzcashbtn" runat="server" Text="Pay with JazzCash" Width="150" OnClick="btnjcash_Click" Enabled="false" />

                    <asp:Panel ID="Panel1" CssClass="popup img-responsive" runat="server">
                        <div>
                            <asp:Label ID="lblaccountd" runat="server" CssClass="transactiondetail" Text="Transaction Detail"></asp:Label>
                            <div>
                                <label class="lblcol">Merchant Name:</label>
                                <asp:Label ID="lbljcash" runat="server" CssClass="pull-right sp1 bg-success" Text="JazzCash"></asp:Label>
                            </div>
                            <br />
                            <div>
                                <asp:Label ID="lbloinfo" runat="server" CssClass="lblcol" Text="Order Information:" Style="font-weight: 700"></asp:Label>
                                <span class="pull-right sp1 bg-success" id="span2" runat="server"></span>
                            </div>
                            <br />
                            <div class="proPriceView">
                                <asp:Label ID="lbltam" runat="server" Text="Tran" CssClass="lblcol"></asp:Label><asp:Label ID="Label4" runat="server" CssClass="label4" Text="saction Amount:"></asp:Label>
                                <span class="pull-right sp1 bg-success" id="span1" runat="server"></span>
                            </div>
                            <br />
                            <div>
                                <asp:Label ID="lblmob" runat="server" Text="Mobil" CssClass="lblcol" Style="font-weight: 700"></asp:Label><asp:Label ID="Label5" runat="server" CssClass="label4" Style="font-weight: 700" Text="e Number"></asp:Label>
                                <asp:TextBox ID="MobileNumber" runat="server" CssClass="form-control pull-right"></asp:TextBox>

                            </div>
                            <asp:RequiredFieldValidator ID="rfvmob" runat="server" ErrorMessage="Field Empty!"
                                ControlToValidate="MobileNumber" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup" CssClass="bg-primary"></asp:RequiredFieldValidator>
                            <br />
                            <div>
                                <label class="lblcol">Pin Code</label>
                                <asp:TextBox ID="pincode" runat="server" CssClass="form-control pull-right" TextMode="Password" placeholder="pin"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="rfvpin" runat="server" ErrorMessage="Field Empty!"
                                ControlToValidate="pincode" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup" CssClass="bg-primary"></asp:RequiredFieldValidator>
                            <br />
                            <br />
                            <hr />
                            <asp:Button ID="tsub" runat="server" Text="Submit" Class="btn btn-success" ValidationGroup="Popup" OnClick="tsub_Click" />
                            <asp:Button ID="Button2" runat="server" Class="btn btn-primary pull-right" Text="Cancel" OnClick="Button2_Click" />
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="empty" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" TargetControlID="empty" CancelControlID="Button2" PopupControlID="Panel1" BackgroundCssClass="popbackground"
                        runat="server">
                    </ajaxToolkit:ModalPopupExtender>
                </div>
                <div id="wallets" class="tab-pane fade in active">
                    <h3 class="easycolor">EasyPaisa</h3>
                    <p>
                        Easypay Mobile Account can be registered on any network mobile number.<br />
                        <br />
                        <strong>You’re almost done!</strong><br />
                        To change or edit your order, go back. No changes will be allowed once you click "CONFIRM ORDER".
                    </p>
                    <asp:Button ID="btnEasyPaisa" OnClick="btnEasyPaisa_Click" CssClass="easypaisabtn" runat="server" Text="Pay With EasyPaisa" Width="150" Enabled="false" />

                    <asp:Panel ID="pnleasypaisa" CssClass="popupeasypaisa img-responsive" runat="server">
                        <div>
                            <asp:Label ID="Label6" runat="server" CssClass="transactiondetail1" Text="Transaction Detail"></asp:Label>
                            <div>
                                <label class="lblcol">Merchant Name:</label>
                                <asp:Label ID="Label7" runat="server" CssClass="pull-right sp1" Text="EasyPaisa"></asp:Label>
                            </div>
                            <br />
                            <div>
                                <asp:Label ID="Label8" runat="server" Text="Order Information:" Style="font-weight: 700" CssClass="lblcol"></asp:Label>
                                <span class="pull-right sp1" id="span3" runat="server"></span>
                            </div>
                            <br />
                            <div class="proPriceView">
                                <asp:Label ID="Label9" runat="server" Text="Transaction Amount" CssClass="lblcol"></asp:Label>
                                <span class="pull-right sp1" id="span4" runat="server"></span>
                            </div>
                            <br />
                            <div>
                                <asp:Label ID="lblmob1" runat="server" Text="Mobile Number" CssClass="lblcol" Style="font-weight: 700"></asp:Label>
                                <asp:TextBox ID="txtbmob1" runat="server" CssClass="form-control pull-right"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator ID="rfvmob1" runat="server" ErrorMessage="Field Empty!"
                                ControlToValidate="txtbmob1" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup1" CssClass="bg-primary"></asp:RequiredFieldValidator>

                            <br />
                            <div>
                                <label class="lblcol">Pin Code</label>

                                <asp:TextBox ID="txtbpin1" runat="server" CssClass="form-control pull-right" TextMode="Password" placeholder="pin"></asp:TextBox>

                            </div>
                            <asp:RequiredFieldValidator ID="rfvpin1" runat="server" ErrorMessage="Field Empty!"
                                ControlToValidate="txtbpin1" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Popup1" CssClass="bg-primary"></asp:RequiredFieldValidator>

                            <br />
                            <br />
                            <br />
                            <br />

                            <asp:Button ID="submitEP" runat="server" Text="Submit" Class="btn btn-success" ValidationGroup="Popup1" OnClick="submitEP_Click" />
                            <asp:Button ID="btncan" runat="server" Class="btn btn-primary pull-right" Text="Cancel" OnClick="btncan_Click" />
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="empty1" runat="server" />
                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" TargetControlID="empty1" CancelControlID="btncan" PopupControlID="pnleasypaisa" BackgroundCssClass="popbackground"
                        runat="server">
                    </ajaxToolkit:ModalPopupExtender>
                </div>
            </div>

        </div>
    </div>
</asp:Content>

