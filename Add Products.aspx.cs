﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

public partial class Add_Products : System.Web.UI.Page
{
   public static String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCat();
            ddlSubCategory.Enabled = false;
        }
    }

    private void BindCat()
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("select * from tblCategories", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            if (dt.Rows.Count != 0)
            {
                ddlCategory.DataSource = dt;
                ddlCategory.DataTextField = "CatName";
                ddlCategory.DataValueField = "CatID";
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("-Select-", "0"));
            }

        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (txtPName.Text.Equals("") || txtPrice.Text.Equals("") || txtQuantity.Text.Equals("") || txtSelPrice.Text.Equals("") || txtSelPrice.Text.Equals("") || txtDesc.Text.Equals("") || txtMatCare.Text.Equals("") || txtPDetails.Text.Equals(""))
        {
            lblerror.Text = "Fields Required!";
            lblerror.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
           try{
                using (SqlConnection con = new SqlConnection(CS))
                {
                    SqlCommand cmd = new SqlCommand("spsInsert", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PName", txtPName.Text);
                    cmd.Parameters.AddWithValue("@PPrice", txtPrice.Text);
                    cmd.Parameters.AddWithValue("@PSelPrice", txtSelPrice.Text);
                    cmd.Parameters.AddWithValue("@PCatID", ddlCategory.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("@PSubCatID", ddlSubCategory.SelectedItem.Value);
                    cmd.Parameters.AddWithValue("@PDescription", txtDesc.Text);
                    cmd.Parameters.AddWithValue("@PProductDetails", txtPDetails.Text);
                    cmd.Parameters.AddWithValue("@PMaterialCare", txtMatCare.Text);
                    if (cbFD.Checked == true)
                    {
                        cmd.Parameters.AddWithValue("@FreeDelivery", 1.ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@FreeDelivery", 0.ToString());
                    }
                    if (cb30Ret.Checked == true)
                    {
                        cmd.Parameters.AddWithValue("@30DayRet", 1.ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@30DayRet", 0.ToString());
                    }
                    if (cbCOD.Checked == true)
                    {
                        cmd.Parameters.AddWithValue("@COD", 1.ToString());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@COD", 0.ToString());
                    }
                    con.Open();
                    Int64 PID = Convert.ToInt64(cmd.ExecuteScalar());

                    //Insert Size Quantity

                    for (int i = 0; i < cblSize.Items.Count; i++)
                    {
                        if (cblSize.Items[i].Selected == true)
                        {
                            Int64 SizeID = Convert.ToInt64(cblSize.Items[i].Value);
                            int Quantity = Convert.ToInt32(txtQuantity.Text);

                            SqlCommand cmd2 = new SqlCommand("insert into tblProductSizeQuantity values('" + PID + "','" + SizeID + "','" + Quantity + "')", con);

                            cmd2.ExecuteNonQuery();
                        }

                    }

                    //Insert and upload Images
                    if (fuImg01.HasFile)
                    {
                        string SavePath = Server.MapPath("~/Images/ProductImages/") + PID;
                        if (!Directory.Exists(SavePath))
                        {
                            Directory.CreateDirectory(SavePath);
                        }
                        string Extention = Path.GetExtension(fuImg01.PostedFile.FileName);
                        fuImg01.SaveAs(SavePath + "\\" + txtPName.Text.ToString().Trim() + "01" + Extention);

                        SqlCommand cmd3 = new SqlCommand("insert into tblProductImages values('" + PID + "','" + txtPName.Text.ToString().Trim() + "01" + "','" + Extention + "')", con);
                        cmd3.ExecuteNonQuery();
                        lblerror.Text = "Product Added Successfully";
                        lblerror.ForeColor = System.Drawing.Color.Green;
                        txtDesc.Text = "";
                        txtMatCare.Text = "";
                        txtPDetails.Text = "";
                        txtPName.Text = "";
                        txtPrice.Text = "";
                        txtQuantity.Text = "";
                        txtSelPrice.Text = "";
                    }
                    if (fuImg02.HasFile)
                    {
                        string SavePath = Server.MapPath("~/Images/ProductImages/") + PID;
                        if (!Directory.Exists(SavePath))
                        {
                            Directory.CreateDirectory(SavePath);
                        }
                        string Extention = Path.GetExtension(fuImg02.PostedFile.FileName);
                        fuImg02.SaveAs(SavePath + "\\" + txtPName.Text.ToString().Trim() + "02" + Extention);

                        SqlCommand cmd4 = new SqlCommand("insert into tblProductImages values('" + PID + "','" + txtPName.Text.ToString().Trim() + "02" + "','" + Extention + "')", con);
                        cmd4.ExecuteNonQuery();
                        lblerror.Text = "Product Added Successfully";
                        lblerror.ForeColor = System.Drawing.Color.Green;
                        txtDesc.Text = "";
                        txtMatCare.Text = "";
                        txtPDetails.Text = "";
                        txtPName.Text = "";
                        txtPrice.Text = "";
                        txtQuantity.Text = "";
                        txtSelPrice.Text = "";
                    }
                    if (fuImg03.HasFile)
                    {
                        string SavePath = Server.MapPath("~/Images/ProductImages/") + PID;
                        if (!Directory.Exists(SavePath))
                        {
                            Directory.CreateDirectory(SavePath);
                        }
                        string Extention = Path.GetExtension(fuImg03.PostedFile.FileName);
                        fuImg03.SaveAs(SavePath + "\\" + txtPName.Text.ToString().Trim() + "03" + Extention);

                        SqlCommand cmd5 = new SqlCommand("insert into tblProductImages values('" + PID + "','" + txtPName.Text.ToString().Trim() + "03" + "','" + Extention + "')", con);
                        cmd5.ExecuteNonQuery();
                        lblerror.Text = "Product Added Successfully";
                        lblerror.ForeColor = System.Drawing.Color.Green;
                        txtDesc.Text = "";
                        txtMatCare.Text = "";
                        txtPDetails.Text = "";
                        txtPName.Text = "";
                        txtPrice.Text = "";
                        txtQuantity.Text = "";
                        txtSelPrice.Text = "";
                    }
                    if (fuImg04.HasFile)
                    {
                        string SavePath = Server.MapPath("~/Images/ProductImages/") + PID;
                        if (!Directory.Exists(SavePath))
                        {
                            Directory.CreateDirectory(SavePath);
                        }
                        string Extention = Path.GetExtension(fuImg04.PostedFile.FileName);
                        fuImg04.SaveAs(SavePath + "\\" + txtPName.Text.ToString().Trim() + "04" + Extention);

                        SqlCommand cmd6 = new SqlCommand("insert into tblProductImages values('" + PID + "','" + txtPName.Text.ToString().Trim() + "04" + "','" + Extention + "')", con);
                        cmd6.ExecuteNonQuery();
                        lblerror.Text = "Product Added Successfully";
                        lblerror.ForeColor = System.Drawing.Color.Green;
                        txtDesc.Text = "";
                        txtMatCare.Text = "";
                        txtPDetails.Text = "";
                        txtPName.Text = "";
                        txtPrice.Text = "";
                        txtQuantity.Text = "";
                        txtSelPrice.Text = "";
                    }
                    if (fuImg05.HasFile)
                    {
                        string SavePath = Server.MapPath("~/Images/ProductImages/") + PID;
                        if (!Directory.Exists(SavePath))
                        {
                            Directory.CreateDirectory(SavePath);
                        }
                        string Extention = Path.GetExtension(fuImg05.PostedFile.FileName);
                        fuImg05.SaveAs(SavePath + "\\" + txtPName.Text.ToString().Trim() + "05" + Extention);

                        SqlCommand cmd7 = new SqlCommand("insert into tblProductImages values('" + PID + "','" + txtPName.Text.ToString().Trim() + "05" + "','" + Extention + "')", con);
                        cmd7.ExecuteNonQuery();
                        lblerror.Text = "Product Added Successfully";
                        lblerror.ForeColor = System.Drawing.Color.Green;
                        txtDesc.Text = "";
                        txtMatCare.Text = "";
                        txtPDetails.Text = "";
                        txtPName.Text = "";
                        txtPrice.Text = "";
                        txtQuantity.Text="";
                        txtSelPrice.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                lblerror.Text = (ex.Message);
                lblerror.ForeColor = System.Drawing.Color.Red;
            }

        }
      
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        int MainCategoryID = Convert.ToInt32(ddlCategory.SelectedItem.Value);

        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("select * from tblSubCategories where MainCatID='" + ddlCategory.SelectedItem.Value + "'", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            if (dt.Rows.Count != 0)
            {
                ddlSubCategory.DataSource = dt;
                ddlSubCategory.DataTextField = "SubCatName";
                ddlSubCategory.DataValueField = "SubCatID";
                ddlSubCategory.DataBind();
                ddlSubCategory.Items.Insert(0, new ListItem("-Select-", "0"));
                ddlSubCategory.Enabled = true;
            }

        }
    }
    protected void ddlSubCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("select * from tblSizes where SubCategoryID='" + ddlSubCategory.SelectedItem.Value + "'and CategoryID='" + ddlCategory.SelectedItem.Value + "'", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            if (dt.Rows.Count != 0)
            {
                cblSize.DataSource = dt;
                cblSize.DataTextField = "SizeName";
                cblSize.DataValueField = "SizeID";
                cblSize.DataBind();
                
            }

        }
    }
}