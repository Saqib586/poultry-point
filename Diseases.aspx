﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Diseases.aspx.cs" Inherits="Diseases" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Poultry Point</title>
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/Custom-Cs.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        $(document).ready(function myfunction() {
            $("#btnCart").click(function myfunction() {
                window.location.href = "/Cart.aspx";
            });
        });
    </script>

</head>
<body>
     <div style="background: url('Images/dis.jpg') no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    height:100%;">
    <form id="form1" runat="server">
        <div>

            <!-- Navigation -->

            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><span>
                            <img alt="logo" src="Images/logo.jpg" height="30" /></span>Poultry Point</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="Default.aspx">Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Products.aspx">All Products</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="Categories.aspx">Categories</a></li>
                                </ul>
                            </li>
                            <li class="active"><a href="Diseases.aspx">Diseases</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Mission.aspx">Mission</a></li>
                                    <li role="separator" class="divider"></li>
                                <li><a href="Vision.aspx">Vision</a></li>
                                </ul>
                            </li>
                            <li><a href="ContactUs.aspx">Contact Us</a></li>
                            <li>
                                <button id="btnCart" class="btn btn-primary navbar-btn" type="button">
                                    Cart <span class="badge" id="pCount" runat="server"></span>
                                </button>
                            </li>
                            <li id="btnSignUp" runat="server"><a href="SignUp.aspx">Sign Up</a></li>
                            <li id="btnSignIn" runat="server"><a href="SignIn.aspx">Sign In</a></li>
                            <li>
                                <asp:Button ID="btnSignOut" Class="btn btn-default navbar-btn" runat="server" Text="Sign Out" OnClick="btnSignOut_Click" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Navigation -->
        </div>
        <!-- Middle contents -->


        <div style="padding-top: 30px;" class="container center ">
            <h1 class="head aboutus">Diseases</h1>
            <h4>Serious illness is unlikely in a backyard flock, especially if you vaccinate the chickens. All the same, it’s good to be aware of them in case you ever are wondering, is my chicken sick? Diseases can spread from wild birds and pests, so keep an eye out during your daily health checks for the symptoms listed below.</h4>

            <div class="row">
                <div class="col-lg-4">
                    <img class="img-rounded" src="Images/Diseases/170px-BatteryChicken5DaysOutOfCage.jpg" alt="pic1" width="140" height="140" />
                    <h2>Avian Pox/Fowl Pox</h2>
                    <h4>Symptoms</h4>
                    <p>White spots on skin, combs turn into scabby sores, white membrane and ulcers in mouth, on trachea, laying stops, all ages affected.</p>
                    <h4>How contracted</h4>
                    <p>Viral disease, mosquitoes, other chickens with pox and contaminated surfaces.</p>
                </div>
                <div class="col-lg-4">
                    <img class="img-rounded" src="Images/Diseases/31c456e2_WP_20130219_069.jpeg" alt="pic2" width="140" height="140" />
                    <h2>Botulism</h2>
                    <h4>Symptoms</h4>
                    <p>Tremors quickly progressing to paralysis of body, including breathing feathers pull out easily death in a few hours.</p>
                    <h4>How contracted</h4>
                    <p>Caused by a bacterial byproduct and by eating or drinking botulism-infected food or water.</p>
                </div>
                <div class="col-lg-4">
                    <img class="img-rounded" src="Images/Diseases/32_1.jpg" alt="pic3" width="140" height="140" />
                    <h2>Fowl Cholera</h2>
                    <h4>Symptoms</h4>
                    <p>Usually birds over 4 months-greenish yellow diarrhea breathing difficulty swollen joints darkened head and wattles often quick death. Does not infect humans.</p>
                    <h4>How contracted</h4>
                    <p>Bacterial disease wild birds, raccoons, opossums, rats, can carry. Also transmitted bird to bird and on contaminated soil, equipment, shoes, clothing contaminated water and food.</p>
                </div>
                 <div class="col-lg-4">
                    <img class="img-rounded" src="Images/Diseases/botulism.jpg" alt="pic3" width="140" height="140" />
                    <h2>Infectious Bronchitis</h2>
                    <h4>Symptoms</h4>
                    <p>Coughing, sneezing, watery discharge from nose and eyes, hens stop laying.</p>
                    <h4>How contracted</h4>
                    <p>Viral disease, highly contagious, spreads through air, contact, and contaminated surfaces.</p>
                </div>
                 <div class="col-lg-4">
                    <img class="img-rounded" src="Images/Diseases/Mareks-2-Small.jpg" alt="pic4" width="140" height="140" />
                    <h2>Mareks</h2>
                    <h4>Symptoms</h4>
                    <p>Affects birds under 20 weeks primarily; causes tumors externally and internally; paralysis; iris of eye turns gray, doesn’t react to light.</p>
                    <h4>How contracted</h4>
                    <p>Viral disease; very contagious; contracted by inhaling shed skin cells or feather dust from other infected birds.</p>
                </div>
                 <div class="col-lg-4">
                    <img class="img-rounded" src="Images/Diseases/Disease_of_roosters_in_Mozambique_(4379534306).jpg" alt="pic5" width="140" height="140" />
                    <h2>Mycoplasmosis</h2>
                    <h4>Symptoms</h4>
                    <p>Mild form — weakness and poor laying. Acute form — breathing problems, coughing, sneezing, swollen infected joints and death.</p>
                    <h4>How contracted</h4>
                    <p>Mycoplasma disease; contracted through other birds (wild birds carry it); can transmit through egg to chick from infected hen.</p>
                </div>
                 <div class="col-lg-4">
                    <img class="img-rounded" src="Images/Diseases/infectious-bronchitis_large.jpg" alt="pic6" width="140" height="140" />
                    <h2>Moniliasis (Thrush)</h2>
                    <h4>Symptoms</h4>
                    <p>White cheesy substance in crop; ruffled feathers; droopy looking; poor laying; white crusty vent area; inflamed vent area; increased appetite.</p>
                    <h4>How contracted</h4>
                    <p>Fungal disease; contracted through moldy feed and water and surfaces contaminated by infected birds. Often occurs after antibiotic treatment for other reasons.</p>
                </div>
            </div>
        </div>
        <!-- Middle contents -->

        <!-- Footer -->


        <footer class="panel-footer navbar-fixed-bottom">
            <div class="container lblcol">
                <p class="pull-right"><a href="#">Back on top</a> </p>
                <p>Poultry Point&trade; 2018 &middot; <a href="Default.aspx">Home</a> &middot; <a href="#">About Us</a> &middot; <a href="ContactUs.aspx">Contact Us</a> &middot; <a href="Products.aspx">Shop</a> &middot;</p>
            </div>
        </footer>
        <!-- Footer -->

    </form>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

         </div>
</body>
</html>
