﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="AdminHome.aspx.cs" Inherits="AdminHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Welcome Admin !</h1>
    <asp:Button ID="btnProductAdd" CssClass="btn btn-warning" runat="server" Text="Add Product" OnClick="btnProductAdd_Click" />
    <asp:Button ID="btnCatadd" CssClass="btn btn-warning" runat="server" Text="Add Category" OnClick="btnCatadd_Click" />
    <asp:Button ID="btnSubCatadd" CssClass="btn btn-warning" runat="server" Text="Add SubCategory" OnClick="btnSubCatadd_Click"  />
    <asp:Button ID="btnSizeadd" CssClass="btn btn-warning" runat="server" Text="Add Size" OnClick="btnSizeadd_Click"  />

</asp:Content>

