﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RecoverPassword.aspx.cs" Inherits="RecoverPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Reset your Password</title>
    
     <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/Custom-Cs.css" rel="stylesheet" />
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="sweetalert.js" type="text/javascript"></script>
    <script>
        function alertme4() {
            swal(
 'Good job!',
 'Password Reset',
 'success'
)
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <!--NAVBAR-->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><span>
                            <img alt="logo" src="Images/logo.jpg" height="30" /></span>Poultry Point</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="Default.aspx">Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Products.aspx">All Products</a></li>
                                    <li role="separator" class="divider"></li>
                                   <li><a href="Categories.aspx">Categories</a></li>
                                </ul>
                            </li>
                            <li><a href="Diseases.aspx">Diseases</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us<b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="Mission.aspx">Mission</a></li>
                                    <li role="separator" class="divider"></li>
                                <li><a href="Vision.aspx">Vision</a></li>
                                </ul>
                            </li>
                            <li><a href="ContactUs.aspx">Contact Us</a></li>
                            
                            <li><a href="SignIn.aspx">Sign In</a></li>
                            </ul>
                    </div>
                </div>
            </div>
    <!--NAVBAR-->
    </div>
        <div class="container">
            <div class="form-horizontal">
                <h3>Reset Password</h3>
                <hr />
                    <div class="form-group">
                    <asp:Label ID="lblMsg" runat="server"  CssClass="col-md-2 control-label" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
                    </div>
               
                <div class="form-group">
                    <asp:Label ID="lbNewPass" runat="server" Text="Enter New Password" CssClass="col-md-2 control-label" Visible="False"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox ID="tbNewPass"  runat="server" CssClass="form-control" TextMode="Password" Visible="False"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorNewPass" runat="server" ErrorMessage="Please enter your New Password !" Visible="true" CssClass="text-danger" ControlToValidate="tbNewPass"></asp:RequiredFieldValidator>
                    </div>                    
                </div>
                <div class="form-group">
                    <asp:Label ID="lbConPass" runat="server" Text="Confirm Password" CssClass="col-md-2 control-label" Visible="False"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox ID="tbConPass"  runat="server" CssClass="form-control" TextMode="Password" Visible="False"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidatorNewPass" runat="server" CssClass="text-danger" ErrorMessage="Both Passwords must be matched !" ControlToCompare="tbConPass" ControlToValidate="tbNewPass" ></asp:CompareValidator>
                    </div>                    
                </div>
                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-6">
                        <asp:Button ID="btnRecPass" runat="server" Text="Reset" Class="btn btn-primary" Visible="False" OnClick="btnRecPass_Click" />
                    </div>
                </div>
                <div class="passrec"><a  href="SignIn.aspx" id="signref" runat="server"  Visible="false" >GOTO SIGNIN PAGE</a></div>

            </div>
        </div>
    </form>
<!-- Footer -->
        <hr />
    <div class="divDet2"></div>
    <div class="divDet2"></div>
    <div class="divDet2"></div>
    <div class="divDet2"></div>
        <footer class="panel-footer navbar-fixed-bottom">
            <div class="container lblcol">
            <p class="pull-right"><a href="#"> Back on top</a> </p>
                <p>Poultry Point&trade; 2018 &middot; <a href="Default.aspx">Home</a> &middot; <a href="#">About Us</a> &middot; <a href="ContactUs.aspx">Contact Us</a> &middot; <a href="Products.aspx">Shop</a> &middot;</p>
        </div>
                </footer>
        <!-- Footer -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
