﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;

public partial class RecoverPassword : System.Web.UI.Page
{
    String CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
    String GUIDvalue;
    DataTable dt = new DataTable();
    int Uid;

    protected void Page_Load(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            GUIDvalue = Request.QueryString["Uid"];
            if (GUIDvalue != null)
            {
                SqlCommand cmd = new SqlCommand("select * from ForgotPassRequests where id='" + GUIDvalue + "'", con);
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                if (dt.Rows.Count != 0)
                {
                    Uid = Convert.ToInt32(dt.Rows[0][1]);
                }
                else
                {
                    lblMsg.Text = "Your Password Reset link is Expired or Invalid !";
                    lblMsg.ForeColor = Color.Red;
                    lbNewPass.Visible = false;
                    tbNewPass.Visible = false;
                    lbConPass.Visible = false;
                    tbConPass.Visible = false;
                    btnRecPass.Visible = false;
                    signref.Visible = true;
                    RequiredFieldValidatorNewPass.Visible = false;
                }

            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            if (dt.Rows.Count != 0)
            {
                lbNewPass.Visible = true;
                tbNewPass.Visible = true;
                lbConPass.Visible = true;
                tbConPass.Visible = true;
                btnRecPass.Visible = true;
                //gotolbl.Visible = true;

            }
            else
            {
                lblMsg.Text = "Your Password Reset link is Expired or Invalid !";
                lblMsg.ForeColor = Color.Red;
                lbNewPass.Visible = false;
                tbNewPass.Visible = false;
                lbConPass.Visible = false;
                tbConPass.Visible = false;
                btnRecPass.Visible = false;
                signref.Visible = true;
                
            }

        }
    }
    protected void btnRecPass_Click(object sender, EventArgs e)
    {
        if (tbNewPass.Text != "" && tbConPass.Text != "" && tbConPass.Text == tbNewPass.Text)
        {
            using (SqlConnection con = new SqlConnection(CS))
            {
                SqlCommand cmd = new SqlCommand("update Users set Password='" + tbNewPass.Text + "' where Uid='" + Uid + "'", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("delete from ForgotPassRequests where Uid='" + Uid + "'", con);
                cmd2.ExecuteNonQuery();
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "ok", "alertme4()", true);
                //Response.Redirect("~/SignIn.aspx");
                lbNewPass.Visible = false;
                tbNewPass.Visible = false;
                lbConPass.Visible = false;
                tbConPass.Visible = false;
                btnRecPass.Visible = false;
                signref.Visible = true;

            }
        }
    }
}