﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User.master" AutoEventWireup="true" CodeFile="Categories.aspx.cs" Inherits="Categories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row" style="padding-top: 40px;">
        <asp:Repeater ID="rptrCategories" runat="server">
            <ItemTemplate>
                <div class="col-lg-3">
                    <a style="text-decoration: none;" href="Products.aspx">
                        <div class="img-thumbnail thumbnail thmbimg">
                            <img src="Images/<%#Eval("CatIMG") %><%#Eval("Extension") %>" alt="pic1" />
                            <div class="caption">
                                <div class="probrand"><%#Eval("CatName") %></div>
                            </div>
                        </div>
                    </a>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Content>


