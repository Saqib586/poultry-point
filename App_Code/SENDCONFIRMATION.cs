﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.IO;
/// <summary>
/// Summary description for SENDCONFIRMATION
/// </summary>
public class SENDCONFIRMATION
{
    
        public String SendMessage(int userId)
        {

            string status = "";
            string updatephone = "";
            string PhoneNumber;
             string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
             using (SqlConnection conn = new SqlConnection(CS))
             {
                 try
                 {
                     conn.Open();
                     SqlCommand sc = new SqlCommand("sendMessage", conn);
                    
                
                     sc.Parameters.AddWithValue("userid", userId);

                     sc.CommandType = CommandType.StoredProcedure;
                     SqlDataAdapter sda = new SqlDataAdapter(sc);
                     DataSet ds = new DataSet();
                     sda.Fill(ds);
                     if (ds.Tables[0].Rows.Count > 0)
                     {
                         string msg = "Tracking ID: " + ds.Tables[0].Rows[0][0].ToString() + "\n" + "Name: " + ds.Tables[0].Rows[0][1].ToString() + "\n"+ "Address: "+ ds.Tables[0].Rows[0][2].ToString()+"\n"+"Your shippment is underway it takes 4 to 5 working days!";
                         PhoneInitials pi = new PhoneInitials();
                         updatephone = pi.decidePhoneInitials(ds.Tables[0].Rows[0][3].ToString());
                         try
                         {
                             PhoneNumber = "+92" + updatephone;
                             WebClient webclient = new WebClient();
                             Stream s = webclient.OpenRead(string.Format("https://platform.clickatell.com/messages/http/send?apiKey=ZecyK38gTV-DrU1P91lmUQ==&to=" + PhoneNumber + "&content=" + msg));
                             StreamReader reader = new StreamReader(s);
                             string result = reader.ReadToEnd();



                             char[] resultCharacters = result.ToCharArray();
                             string add = "";
                             List<string> returnMessage = new List<string>();
                             for (int i = 0; i < result.Length; i++)
                             {
                                 if (result[i] != '{' && result[i] != '[' && result[i] != '\"')
                                 {
                                     if (result[i] != ':' && result[i] != ',')
                                     {
                                         add += result[i];
                                     }
                                     else
                                     {
                                         returnMessage.Add(add);
                                         add = "";
                                     }
                                 }

                             }
                             if (returnMessage[returnMessage.IndexOf("error") + 1] == "null")
                             {
                                 status = msg;
                             }
                             else
                             {
                                 status = "Failed";
                             }
                         }
                         catch (Exception exp)
                         {
                             status = exp.Message;
                         }

                     }
                     else
                     {
                         status = "Not Found!";
                     }

                 }
                 catch (Exception exp)
                 {
                     status = exp + "Connection Issue";
                 }
                 return status;

             }
        }
}